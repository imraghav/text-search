from django.conf.urls import url

from . import views

urlpatterns = [
    url('home/', views.index, name='index'),
    url('all_docs/', views.all_docs, name='all_docs'),
    url('document/',views.enter_doc, name='enter_doc'),
    url('search/$',views.search_doc, name='search_doc'),
    #url('result/$', views.result, name='result'),
    #url('result/',views.result, name='result'),
]