from models import Document

def search(text):
	'''
	'''
	searched_docs = []
	tokenized_text = tokenize(text)
	normalized_text = normalize(tokenized_text)
	for n in normalized_text:
		s = Document.objects.filter(text__contains=n).values('id', 'text')
		if s not in searched_docs:
			searched_docs.append(s)
	print(searched_docs) 
	return searched_docs

def tokenize(text):
	text_tokenized = text.split(" ")
	return text_tokenized

def normalize(text):
	n = [str(x).lower() for x in text]
	return n
