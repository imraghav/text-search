from django import forms

class DocumentForm(forms.Form):
	id = forms.CharField(label='Document ID', max_length=10)
	text = forms.CharField(label='Document Text', max_length=200)

class SearchForm(forms.Form):
	text = forms.CharField(label='Enter text to search in Documents', max_length=200)