# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models

# Create your models here.

class Document(models.Model):
	id = models.CharField(max_length=10,primary_key=True)
	text = models.CharField(max_length=200)

	def __str__(self):
		return self.text