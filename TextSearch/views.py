# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.shortcuts import render
from django.http import HttpResponse,HttpResponseRedirect
from .models import Document
from django.shortcuts import render, redirect
from TextSearch.models import Document
from .forms import DocumentForm, SearchForm
from search_engine import search
import json

def index(request):
	context = {}
	return render(request, 'TextSearch/index.html', context)

def all_docs(request):
	docs = Document.objects.all()
	context = {
        'all_docs': docs,
    }
	return render(request, 'TextSearch/all_docs.html', context)

def enter_doc(request):
	if request.method=='POST':
		id = request.POST['id']
		text = request.POST['text']
		q = Document(text=text,id=id)
		q.save()
		return HttpResponseRedirect('all_docs/')
	else:
		form = DocumentForm()
	
	return render(request, 'TextSearch/document.html', {'form': form})

def search_doc(request):
	if request.GET:
		form = SearchForm(request.GET)
		if form.is_valid():
			final_dict = {}
			texts = []
			count = 0
			text = form.cleaned_data['text']
			searched_docs = search(text)
			for queryset in searched_docs:
				for doc in queryset:
					count = count+1
					texts.append(doc)
			final_dict['count'] = count
			final_dict['documents'] = texts
			final_dict = json.dumps(final_dict)
			return render(request, 'TextSearch/result.html', {'result': final_dict})

	else:
		form = SearchForm()

	return render(request, 'TextSearch/search_doc.html', {'form': form})



