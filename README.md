# Full Text Search

After cloning the repository follow the following steps to run the project:

1. Install required packages using:```pip install -r requirements.txt```

2. Go into the base directory of the project and run Server using- ```python manage.py runserver```

3. Open the browser and visit: [http://127.0.0.1:8000/textsearch/home/]

4. The endpoint for submitting the document is: [http://127.0.0.1:8000/textsearch/document/]

5. The endpoint for searching the document is: [http://127.0.0.1:8000/textsearch/search/] 

